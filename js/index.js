$(function () {
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
  $(".carousel").carousel({
    interval: 2500,
  });
  $("#contacto").on("show.bs.modal", function (e) {
    $("#btnContacto").removeClass("btn-primary");
    $("#btnContacto").removeClass("btn-product");
    $("#btnContacto").addClass("btn-outline-success");
    $("#btnContacto1").removeClass("btn-primary");
    $("#btnContacto1").removeClass("btn-product");
    $("#btnContacto1").addClass("btn-outline-success");
    console.log("el modal se muestra");
  });
  $("#contacto").on("shown.bs.modal", function (e) {
    $("#btnContacto").addClass("btn-primary");
    $("#btnContacto").addClass("btn-product");
    $("#btnContacto").removeClass("btn-outline-success");
    $("#btnContacto1").addClass("btn-primary");
    $("#btnContacto1").addClass("btn-product");
    $("#btnContacto1").removeClass("btn-outline-success");
    console.log("el modal se mostro");
  });
  $("#contacto").on("hide.bs.modal", function (e) {
    console.log("el modal se esta ocultando");
  });
  $("#contacto").on("hidden.bs.modal", function (e) {
    console.log("el modal se oculto");
  });
});
